
public class Snake {

	private int length =5;
	private DoublyLinkedList<Square> dll2 = new DoublyLinkedList<Square>();
	private int xPos = 800;
	private int yPos = 350;
	private Square vierkant;
	private Square temp;
	private int direction;
	
	/**
	 * this "direction" integer has 4 values, each designated to a certain direction
	 * 0 = up , 1 = left, 2 = down, 3 = right
	 */

public Snake() {

	
	dll2.addFirst(vierkant= new Square (xPos, yPos));
	for(int i = 1; i < length;i++) {
	yPos = yPos+25;
	dll2.addLast(vierkant = new Square (xPos, yPos));}
}
public int getDirection() {
	return direction;
}

public void setLenght(int length) {
	this.length= length;}
	
	public void makeSnakeLonger() {
		length = dll2.size() + 1;}
	
public boolean checkBorderCollision() {
	temp =dll2.getHead();
	int xP = temp.getX();
	int yP = temp.getY();
	if(xP < 25 || xP > 975 || yP < 100 || yP > 900) {return true;}
	else {return false;}
	}
	
	


public void moveForward() {
if(direction != 2 && checkBorderCollision() == false) {
int tempY = 0;
dll2.removeLast();
temp = dll2.getHead();
tempY = temp.getY();
yPos = tempY -25;
xPos = temp.getX();
dll2.addFirst(vierkant = new Square (xPos, yPos) );
direction = 0;
slaap();}
else {}
}

public void moveBackward() {
	if(direction !=0 && checkBorderCollision() == false) {
int tempY = 0;
dll2.removeLast();
temp = dll2.getHead();
tempY = temp.getY();
yPos = tempY + 25;
xPos = temp.getX();
dll2.addFirst(vierkant = new Square (xPos, yPos));
direction = 2;
slaap();}
else {}
}
public void moveLeft() {
if(direction != 3 && checkBorderCollision() == false) {
int tempX = 0;
dll2.removeLast();
temp = dll2.getHead();
tempX = temp.getX();
yPos = temp.getY();
xPos = tempX -25;
dll2.addFirst(vierkant = new Square (xPos, yPos));
direction = 1;
slaap();}
else {}
}
public void moveRight() {
if(direction != 1 && checkBorderCollision() == false) {
int tempX = 0;
dll2.removeLast();
temp = dll2.getHead();
tempX = temp.getX();
xPos = tempX + 25;
yPos = temp.getY();
dll2.addFirst(vierkant = new Square (xPos, yPos));
direction = 3;
slaap();}
else {}
}

private void slaap() {
    try { Thread.sleep( 500 ); }
    catch( InterruptedException e ) { }
  }
	

}

