import java.awt.*;
import javax.swing.*;
import java.util.*;



public class Ball {
 private int centerX =500;
 private int centerY =400;
 private boolean padCollision;
 private boolean snakeCollision;
 private boolean borderWinCollision;
 private boolean borderCollision;
 private int direction;
 /**
	 * this "direction" integer has 4 values, each designated to a certain direction
	 * 0 = up , 1 = left, 2 = down, 3 = right
	 */

 public void tekenBall(Graphics g) {
	 g.setColor(Color.red);
	      g.fillOval( ( centerX - 25 ), ( centerY - 25 ), 25, 25 );
	    }
public boolean checkWinCollision() {
	if(centerX < 25 || centerX > 975 ) { return true;}
	else {return false;}
	}
public boolean checkBorderCollision() {
		if(centerX < 25 || centerX > 975 || centerY < 100 || centerY > 900 ) { 
		borderCollision = true;
		return true;}
			
	else {return false;}
}
public void setBorderCollisionFalse(boolean a) {
	borderCollision = a;
}
public void setSnakeCollision(boolean a) {
	snakeCollision = a;
}
public void setPadCollision(boolean a) {
	padCollision = a;
}
public int getCenterX() {
 return centerX;	
}

public int getCenterY() {	
	return centerY;	
}
public int getDirection() {
	return direction;
}

public void moveLeft() {
	if(checkWinCollision() == false && direction == 1) {
	centerX= centerX -10;
	slaap();
	direction = 1;}
	else {}
}
public void moveRight() {
	if(checkWinCollision() == false && direction == 3) {
	centerX= centerX +10;
	slaap();
	direction = 3;}
	else {}
}
public void moveUp() {
	 
		if(borderCollision== false && direction ==0) {
	centerY= centerY -10;
	slaap();
	direction = 0;}
		else {direction = 2;
		borderCollision=false;} 
}
public void moveDown() {
		if(borderCollision== false && direction ==2) {
			centerY= centerY +10;
			slaap();
			direction = 2;}
				else {direction = 0;
				borderCollision=false;}
}

private void slaap() {
    try { Thread.sleep( 150 ); }
    catch( InterruptedException e ) { }
  }
}

