import java.awt.*;
public class Square  {

int xPos;
int yPos;
int size = 25;

 public Square() {
  xPos= 0;  
  yPos=0;}
 
 public Square(int x, int y) {
	 this.xPos = x;
	 this.yPos = y;

 }
 
 
  public void setX(int a) {
	  a = xPos;
  }
  public void setY (int b) {
	  b = yPos;  
  }
  public int getX() {
	  return xPos;
  }
  public int getY() {
	  return yPos; 
  }
  public void drawSquare(Graphics g) {
	  g.fillRect(xPos, yPos, 25, 25);
	  
  }
  
}
