
public class Pad {
	private int length =5;
	private DoublyLinkedList<Square> dll1 = new DoublyLinkedList<Square>();
	private int xPos = 250;
	private int yPos = 350;
	private Square vierkant;
	private Square temp;
	
	
	public Pad () {
				
		dll1.addFirst(vierkant = new Square (xPos, yPos));
		for(int i = 1; i < length;i++) {
		yPos = yPos +25;
		vierkant.setY(yPos);
		dll1.addLast(vierkant = new Square (xPos, yPos));}
		
	}
	public boolean checkCollisionUpper() {
		int tempX;
		tempX = dll1.getHead().getY();
		if(tempX == 100) {
			return true;
		}
		else {return false;}
	}
	
	
	public boolean checkCollisionLower() {
		int tempX;
		tempX = dll1.getTail().getY();
		if(tempX == 900) {
			return true;
		}
		else {return false;}
	}
	public void moveUp() {
		if (checkCollisionUpper() == false) {
		int tempY = 0;
		dll1.removeLast();
		temp = dll1.getHead();
		tempY = temp.getY();
		yPos = tempY - 25;
		xPos = temp.getX();
		dll1.addFirst(vierkant = new Square (xPos, yPos));
		slaap();}
		else {}
		}
	
	public void moveDown() {
		if (checkCollisionLower() == false) {
		int tempY = 0;
		dll1.removeFirst();
		temp = dll1.getTail();
		tempY = temp.getY();
		yPos = tempY +25;
		xPos = temp.getX();
		dll1.addFirst(vierkant = new Square (xPos, yPos));
		slaap();}
		else {}
	}
		
	
	private void slaap() {
	    try { Thread.sleep( 500 ); }
	    catch( InterruptedException e ) { }
	  }
}